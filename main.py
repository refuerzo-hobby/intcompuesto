class CompoundInterestSimulator:
    def __init__(self, init_capital, int_rate, desired_amount_):
        """
        Constructor for CompoundInterestSimulator class.

        Parameters:
        - initial_capital (float): Initial amount of money.
        - interest_rate (float): Annual interest rate (decimal).
        - desired_amount (float): Target amount to reach.

        Initializes instance variables for the simulator.
        """
        self.initial_capital = init_capital
        self.interest_rate = int_rate
        self.desired_amount = desired_amount_
        self.days_needed = 1
        self.accumulated_gain = 0.0

    @staticmethod
    def calculate_daily_gain(capital, rate):
        """
        Calculate the daily gain based on the provided capital and interest rate.

        Parameters:
        - capital (float): Current amount of money.
        - rate (float): Daily interest rate.

        Returns:
        float: Daily gain.
        """
        return capital * (rate / 360)

    @staticmethod
    def convert_days_to_years_months_days(days):
        """
        Convert the total number of days into years, months, and remaining days.

        Parameters:
        - days (int): Total number of days.

        Returns:
        tuple: Number of years, months, and remaining days.
        """
        years = days // 360
        months = (days % 360) // 30
        remaining_days = (days % 360) % 30
        return years, months, remaining_days

    def calculate_gain_for_one_month(self, initial_capital, interest_rate):
        """
        Calculate the gain for one month (30 days).

        Parameters:
        - initial_capital (float): Initial amount of money.
        - interest_rate (float): Annual interest rate (decimal).

        Returns:
        float: Gain for one month.
        """
        total_gain = 0.0
        final_capital = initial_capital
        for _ in range(30):
            daily_gain = self.calculate_daily_gain(initial_capital, interest_rate)
            total_gain += daily_gain
            final_capital += daily_gain
        print(f"\nthe capital increased from {initial_capital:.2f} to {final_capital:.2f} a value of ${total_gain:.2f}")

    def simulate_compound_interest(self):
        """
        Simulate compound interest accumulation until the desired amount is reached.
        Print the results at each step.
        """
        while True:
            daily_gain = self.calculate_daily_gain(self.initial_capital, self.interest_rate)
            final_capital = self.initial_capital + daily_gain
            self.accumulated_gain += daily_gain

            print(f'Day {self.days_needed}: Accumulated gain: {self.accumulated_gain:.2f}, '
                  f'Capital: {final_capital:.2f}')

            self.days_needed += 1
            self.initial_capital = final_capital

            if self.desired_amount < final_capital:
                break

        years, months, remaining_days = self.convert_days_to_years_months_days(self.days_needed)
        print(
            f"\nIt takes {years} years, {months} months, and {remaining_days} days to reach a capital of "
            f"{self.desired_amount:.2f}.")


if __name__ == '__main__':
    # Parameters
    initial_capital = 100000
    interest_rate = 0.08
    desired_amount = 1000000

    # Create an instance of the simulator and run the simulation
    simulator = CompoundInterestSimulator(initial_capital, interest_rate, desired_amount)
    simulator.simulate_compound_interest()
    simulator.calculate_gain_for_one_month(initial_capital, interest_rate)
